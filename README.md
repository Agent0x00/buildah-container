# buildah-container

This project is used to build a docker image that runs buildah and includes sshd to facilitate container-to-container communication. This is to be used in an automated pipeline where a master ci-cd container instructs buildah to build a docker image.